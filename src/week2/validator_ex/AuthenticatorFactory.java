package week2.validator_ex;

import week2.factory.Operation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AuthenticatorFactory {
    private static Properties prop = new Properties();
    static {
        try {
            prop.load(new FileInputStream("src/week2/validator_ex/auth.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Authenticator createAuthenticator() {
        KeyValueKeeper keeper = createKeyValueKeeper();
        Encrypter encrypter = createEncrypter();
        return new Authenticator(keeper, encrypter);

    }

    private static KeyValueKeeper createKeyValueKeeper() {
        KeyValueKeeper keyValueKeeper = null;
        try {
            keyValueKeeper = (KeyValueKeeper) Class.forName(prop.getProperty("KeyValueKeeper")).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return keyValueKeeper;
    }

    private static Encrypter createEncrypter() {
        Encrypter encrypter = null;
        try {
            encrypter = (Encrypter) Class.forName(prop.getProperty("Encrypter")).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypter;
    }
}
