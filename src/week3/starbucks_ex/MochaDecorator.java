package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/28.
 */
public class MochaDecorator extends DecoratorC{
    private double price=22;

    public MochaDecorator(IDrink iDrink) {
        super(iDrink);
    }
    public void setCoffee() {
        System.out.println("摩卡"+price);
    }
    public double getPrice() {
        super.getPrice();
        setCoffee();
        return price;
    }
}
