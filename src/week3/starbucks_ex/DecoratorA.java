package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/27.
 */
public abstract class DecoratorA implements IDrink{
    private double price = 11;
    protected IDrink iDrink;

    public DecoratorA(IDrink iDrink) {
        this.iDrink = iDrink;
    }

    public double getPrice() {
    iDrink.getPrice();
    return price;
    }
}
