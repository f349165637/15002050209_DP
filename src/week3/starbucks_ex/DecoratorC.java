package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/28.
 */
public abstract class DecoratorC implements IDrink{
    private double price = 22;
    protected IDrink iDrink;

    public DecoratorC(IDrink iDrink) {
        this.iDrink = iDrink;
    }

    public double getPrice() {
        iDrink.getPrice();
        return price;
    }
}
