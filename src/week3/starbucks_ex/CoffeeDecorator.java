package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/27.
 */
public class CoffeeDecorator extends DecoratorA {
    private double price=11;

    public CoffeeDecorator(IDrink iDrink) {
        super(iDrink);
    }
    public void setCoffee() {
        System.out.println("咖啡"+price);
    }
    public double getPrice() {
        super.getPrice();
        setCoffee();
        return price;
    }
}
