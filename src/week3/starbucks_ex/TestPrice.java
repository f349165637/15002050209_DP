package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/27.
 */
public class TestPrice {
     public static void main(String[] args) {
          DecoratorA decoratora = new CoffeeDecorator(new Coffee());
          decoratora.getPrice();

          DecoratorB decoratorb = new LatteDecorator(new Latte());
          decoratorb.getPrice();

          DecoratorC decoratorc = new MochaDecorator(new Mocha());
          decoratorc.getPrice();

     }
}
