package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/28.
 */
public abstract class DecoratorB implements IDrink{
    private double price = 33;
    protected IDrink iDrink;

    public DecoratorB(IDrink iDrink) {
        this.iDrink = iDrink;
    }

    public double getPrice() {
        iDrink.getPrice();
        return price;
    }
}
