package week3.starbucks_ex;

/**
 * Created by Xun on 2017/9/28.
 */
public class LatteDecorator extends DecoratorB {
    private double price=33;

    public LatteDecorator(IDrink iDrink) {
        super(iDrink);
    }
    public void setCoffee() {
        System.out.println("拿铁"+price);
    }
    public double getPrice() {
        super.getPrice();
        setCoffee();
        return price;
    }
}
