package week1;

/**
 * Created by Xun on 2017/9/12.
 */
public class TicketMaker {
    private int number=1000;
    private TicketMaker(){

    }
    private static TicketMaker instance=new TicketMaker();

    public static TicketMaker getInstance(){
        return instance;
    }
    public  int getNextTicketNumber(){
        return number++;
    }
}
