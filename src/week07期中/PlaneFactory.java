package week07;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PlaneFactory {
    private static Properties prop = new Properties();
    static {
        try {
            prop.load(new FileInputStream("src/week07/prop.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
