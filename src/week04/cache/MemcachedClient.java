package week04.cache;

public class MemcachedClient {

    public void readMemcache() {
        System.out.println("read from Memcache");
    }
    public void writeMemcache() {
        System.out.println("write to Memcache");
    }
}
