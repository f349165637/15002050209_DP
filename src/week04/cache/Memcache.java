package week04.cache;

public class Memcache implements CacheInterface{
    private MemcachedClient memcachedClient;
    public Memcache(MemcachedClient memcachedClient){
        this.memcachedClient=memcachedClient;
    }
    public void read(){
        this.memcachedClient.readMemcache();
    }
    public void write(){
        this.memcachedClient.writeMemcache();
    }
}
