package week04.cache;

public class Redis implements CacheInterface{
    private Jedis jedis;
    public Redis(Jedis jedis){
        this.jedis=jedis;
    }
    public void read(){
        this.jedis.readJedis();
    }
    public void write(){
        this.jedis.writeJedis();
    }
}

