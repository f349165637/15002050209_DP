package week04.cache;

public interface CacheInterface {
    public void read();
    public void write();
}
