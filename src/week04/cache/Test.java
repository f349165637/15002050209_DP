package week04.cache;

public class Test {
    public static void main(String[] args) {
        CacheInterface cacheInterface1=new Memcache(new MemcachedClient());
        cacheInterface1.read();
        cacheInterface1.write();
        CacheInterface cacheInterface2=new Redis(new Jedis());
        cacheInterface2.read();
        cacheInterface2.write();
    }
}
