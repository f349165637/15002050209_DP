package week06.personcmp;

import java.util.Random;

public class TestPersonCmp {
    public static void main(String[] args) {
        Person[] personArray=new Person[10];
        Random random=new Random();
        for(int i=0;i<personArray.length;i++){
            personArray[i]=new Person("Person"+i,random.nextInt(200),random.nextInt(200));
        }
        for(int i=0;i<personArray.length;i++){
            System.out.println(personArray[i].getName()+":"+personArray[i].getHeight()+":"+personArray[i].getWeight());
        }
        AbstractPersonCmp cmp=new PersonCmpByHeight();
        cmp.sort(personArray);
        System.out.println("排序之后");
        for(int i=0;i<personArray.length;i++){
            System.out.println(personArray[i].getName()+":"+personArray[i].getHeight()+":"+personArray[i].getWeight());
        }
    }
}
